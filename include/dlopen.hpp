#pragma once

#include <algorithm>
#include <cassert>
#include <dlfcn.h>
#include <stdexcept>
#include <string>

struct load_current_t {};
inline constexpr load_current_t load_current{};

/**
 * @brief C++ wrapper for dlopen
 * @details This class is a wrapper for dlopen. It is used to load libraries
 * and get symbols from them.
 */
class DLOpen {
public:
  using native_handle_type = void *;

  constexpr DLOpen() noexcept = default;

  explicit DLOpen(load_current_t) {
    m_handle = dlopen(nullptr, RTLD_NOW | RTLD_GLOBAL);
    if (!m_handle) {
      throw std::runtime_error("Failed to load current binary file. " +
                               std::string(dlerror()));
    }
  }

  /**
   * @brief Load a library from a path
   *
   * @param path The path to the library
   */
  explicit DLOpen(const std::string &path) {
    assert(!std::empty(path) && "DLOpen called with empty path.");

    m_handle = dlopen(std::data(path), RTLD_NOW);
    if (!m_handle)
      throw std::runtime_error{"Failed to load binary file " +
                               std::string{dlerror()}};
  }

  /**
   * @brief Destroy the DLOpen object
   */
  ~DLOpen() {
    if (m_handle)
      dlclose(m_handle);
  }

  /**
   * @brief Prevent copying
   */
  DLOpen(const DLOpen &) = delete;
  DLOpen &operator=(const DLOpen &) = delete;

  DLOpen(DLOpen &&other) noexcept
      : m_handle{std::exchange(other.m_handle, native_handle_type{})} {}

  DLOpen &operator=(DLOpen &&other) noexcept {
    m_handle = std::exchange(other.m_handle, m_handle);
    return *this;
  }

  /**
   * @brief Get a symbol from the library
   *
   * @tparam Func The function type
   * @tparam std::enable_if_t<
   * std::is_pointer_v<Func> &&
   * std::is_function_v<std::remove_pointer_t<Func>>>
   * @param symbol The symbol to get
   * @return Func The symbol
   */
  template <typename Func, typename = std::enable_if_t<
                               std::is_pointer_v<Func> &&
                               std::is_function_v<std::remove_pointer_t<Func>>>>
  Func load(const std::string &symbol) const noexcept {
    assert(!std::empty(symbol) &&
           "DLOpen::load called with an empty symbol name.");
    assert(m_handle && "DLOpen::load called with invalid handle.");

    return reinterpret_cast<Func>(dlsym(m_handle, std::data(symbol)));
  }

  /**
   * @brief Get a symbol from the library
   *
   * @tparam Func The function type
   * @param symbol The symbol to get
   * @return Func* The symbol
   */
  template <typename Func,
            typename = std::enable_if_t<std::is_function_v<Func>>>
  Func *load(const std::string &symbol) const noexcept {
    return load<Func *>(symbol);
  }

  /**
   * @brief Get the native handle
   *
   * @return native_handle_type The native handle
   */
  native_handle_type native_handle() const noexcept { return m_handle; }

private:
  native_handle_type m_handle{};
};
