#pragma once

#include <discordframework/export.h>
#include <discordframework/services/service.h>
#include <dpp/dpp.h>
#include <map>
#include <vector>

namespace DiscordFramework::Services {

class EXPORT_DF SlashCommandsService : public Service {
public:
  /**
   * @brief Initialize the slash commands service
   *
   * @return The service instance
   */
  SlashCommandsService();
  ~SlashCommandsService();

  /**
   * @brief Boot the slash command service
   */
  void boot() override;

  void register_command(dpp::slashcommand &cmd);
  void register_command(dpp::snowflake guild_id, dpp::slashcommand &cmd);

  void sync_commands();

private:
  void on_register_error(const dpp::confirmation_callback_t &callback);

  std::map<dpp::slashcommand_contextmenu_type, std::vector<dpp::slashcommand>>
      m_commands;
  std::map<dpp::snowflake, std::map<dpp::slashcommand_contextmenu_type,
                                    std::vector<dpp::slashcommand>>>
      m_guild_commands;
};

} // namespace DiscordFramework::Services
