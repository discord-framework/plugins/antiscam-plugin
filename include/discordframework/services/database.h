#pragma once

#include <discordframework/export.h>
#include <discordframework/services/service.h>
#include <memory>
#include <pqxx/pqxx>

namespace DiscordFramework::Services {

class EXPORT_DF DatabaseService : public Service {
public:
  /**
   * @brief Initialize the database service
   *
   * @return The service instance
   */
  DatabaseService();
  ~DatabaseService();

  /**
   * @brief Get the Connection object
   *
   * @return pqxx::connection*
   */
  pqxx::connection *connection();

  /**
   * @brief Check if the database is enabled
   *
   * @return bool
   */
  bool isEnabled();

  /**
   * @brief Check if the database is connected
   *
   * @return bool
   */
  bool isConnected();

  /**
   * @brief Boot the database service
   */
  void boot() override;

private:
  // The connection instance to the database
  std::unique_ptr<pqxx::connection> c;

  // The state of the database manager
  bool state = false;
};

} // namespace DiscordFramework::Services
