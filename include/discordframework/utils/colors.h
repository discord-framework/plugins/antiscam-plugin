#pragma once

#define WHITE 16777215
#define BLURPLE 5793266
#define GREYPLE 10070709
#define DARK_BUT_NOT_BLACK 2895667
#define NOT_QUITE_BLACK 2303786
#define GREEN 5763719
#define YELLOW 16705372
#define FUSCHIA 15418782
#define RED 15548997
#define BLACK 2303786
#define BLUE 3447003
#define DARK_BLUE 2123412
#define AQUA 1752220
