{
    files = {
        "src/antiscam.cpp"
    },
    depfiles_gcc = "build/.objs/supportlauncher-plugin/linux/x86_64/release/src/antiscam.cpp.o:   src/antiscam.cpp src/antiscam.h   include/discordframework/core/configuration.h   include/discordframework/utils/regex.h   include/discordframework/utils/../exceptions.h   include/sharedlibs-loader/plugin.h include/discordframework/core/bot.h   include/discordframework/services/service_manager.h   include/discordframework/services/service.h   include/discordframework/plugins-loader/plugin_info.h   include/discordframework/utils/strings.h\
",
    values = {
        "/usr/bin/clang++",
        {
            "-Qunused-arguments",
            "-m64",
            "-fPIC",
            "-O3",
            "-std=c++20",
            "-Iinclude",
            "-isystem",
            "/var/cache/xmake-pkgs/s/spdlog/v1.9.2/ee32bc2537484e9393a3a4997957d53b/include",
            "-isystem",
            "/var/cache/xmake-pkgs/d/dpp/v9.0.16/ed61a5223aa44646a5b349eeec7c7388/include",
            "-isystem",
            "/usr/include/opus",
            "-isystem",
            "/var/cache/xmake-pkgs/c/cpr/1.6.2/c67e0d4d315644d5a10d3b1d73d1f735/include",
            "-isystem",
            "/var/cache/xmake-pkgs/l/libcurl/7.80.0/b920763c0b2d4ed78f0696a1977cbbf3/include",
            "-isystem",
            "/var/cache/xmake-pkgs/a/args/6.3.0/b766ea3bc7dd47aa9e7ea9d609f8ad87/include",
            "-isystem",
            "/var/cache/xmake-pkgs/j/json-schema-validator/2.1.0/f0bbb511cca349c792b82edc6245f8b2/include",
            "-isystem",
            "/var/cache/xmake-pkgs/n/nlohmann_json/v3.10.0/0ba70816089c4a7e9a2d429315affa74/include",
            "-fPIC",
            "-rdynamic",
            "-DNDEBUG"
        }
    }
}