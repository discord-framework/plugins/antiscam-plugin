#include "antiscam.h"

#include <discordframework/utils/strings.h>
#include <nlohmann/json-schema.hpp>
#include <thread>

typedef nlohmann::json json;

/*
 * https://github.com/AerGameChannel/AntiScam-Bot
 * https://github.com/LavenderCantCode/AntiScamLinksDiscord
 */

static json plugin_config = R"(
{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "AntiScam plugin Configuration",
  "properties": {
    "blacklist": {
      "type": "array",
      "items": {
        "type": "string"
      },
      "uniqueItems": true
    },
    "mute_message": {
      "type": "string"
    },
    "audit_message": {
      "type": "string"
    }
  },
  "required": ["blacklist", "mute_message", "audit_message"]
}
)"_json;

AntiScam::AntiScam(Bot *bot, PluginInfo *info) : Plugin(bot, info) {
  m_config = std::make_unique<DiscordFramework::Configuration>(
      this->config_path().string().c_str(), json{{"type", dpp::snowflake{0}}});

  try {
    nlohmann::json_schema::json_validator validator;

    validator.set_root_schema(plugin_config);

    m_blacklist =
        m_config->JSON()->at("blacklist").get<std::vector<std::string>>();
    m_mute_msg = m_config->JSON()->at("mute_message").get<std::string>();
    m_audit_msg = m_config->JSON()->at("audit_message").get<std::string>();

    auto h_message = m_bot->cluster->on_message_create(
        std::bind(&AntiScam::onMessageCreate, this, std::placeholders::_1));

    m_handlers.insert({"message_create", h_message});
  } catch (const std::exception &e) {
    m_bot->cluster->log(
        dpp::ll_warning,
        fmt::format("[SupportLauncher] Error while loading config", e.what()));
  }
}

AntiScam::~AntiScam() {
  if (!m_handlers.empty()) {
    m_bot->cluster->on_message_create.detach(m_handlers["message_create"]);
  }
}

void AntiScam::onMessageCreate(const dpp::message_create_t &event) {
  dpp::message msg = event.msg;
  if (msg.author.is_bot() || !msg.guild_id)
    return;

  if (isScamMessage(msg.content)) {
    // Delete the message
    m_bot->cluster->message_delete(msg.id, msg.channel_id);

    mute_user(msg.guild_id, msg.author.id);

    dpp::message m;
    m.content = m_mute_msg;
    m.channel_id = msg.author.id;
    m_bot->cluster->message_create(m);
  } else {
    if (m_last_msg.content == msg.content && msg.content != "" &&
        m_last_msg.author.id == msg.author.id) {
      m_spam_counter++;
      m_bot->cluster->message_delete(msg.id, msg.channel_id);
    } else {
      m_last_msg = msg;
      m_spam_counter = 0;
    }

    if (m_spam_counter > 1) {
      m_spam_counter = 0;
      mute_user(msg.guild_id, msg.author.id);

      m_bot->cluster->message_delete(m_last_msg.id, m_last_msg.channel_id);
    }
  }
}

time_t AntiScam::get_next_day() {
  time_t t;
  time(&t);

  struct tm *tm = localtime(&t);
  tm->tm_mday += 1;
  t = mktime(tm);
  return t;
}

bool AntiScam::isScamMessage(std::string message) {
  for (auto link : m_blacklist) {
    auto regex = Helpers::PCRE{fmt::format(
        R"(/.* (https?:\/\/)?(www.\.)?({})\/.+[a-z] .*/gim)", link)};

    // If content match with blacklist
    if (regex.match(message)) {
      return true;
    }
  }
  return false;
}

void AntiScam::mute_user(dpp::snowflake guild_id, dpp::snowflake user_id) {
  // Mute the user
  dpp::guild_member gm;
  try {
    gm = dpp::find_guild_member(guild_id, user_id);
  } catch (const std::exception &e) {
  }

  json j = json{{"communication_disabled_until", get_next_day()}};

  // Mute the user
  time_t t = dpp::ts_not_null(&j, "communication_disabled_until");
  gm.set_communication_disabled_until(t);

  m_bot->cluster->set_audit_reason(m_audit_msg).guild_edit_member(gm);
}

PLUGIN(AntiScam);
